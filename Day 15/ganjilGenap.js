// Objectives

//     Mampu menggunakan built in function pada Array seperti .push, .shift, .unshift, dll
//     Mampu membuat function dan mengerti penggunaan parameter dari sebuah function
//     Mampu menggunakan template literals

// RESTRICTION Hanya boleh menggunakan built-in function untuk menambahkan atau mengurangi data dalam array, seperti .shift(), unShift(), push(), dan pop()
// Directions
// Antrian

// Diberikan sebuah function ganjilGenap yang menerima satu parameter plat bertipe string. Parameter plat berisi informasi kumpulan plat dimana nomor antar plat dipisahkan oleh titik koma(;).

// Function ini akan mengembalikan keterangan jumlah plat genap dan jumlah plat ganjil.

function ganjilGenap(plat) {
  if (plat!=undefined){
    let hasil = plat.split(';');
    var genap=0,ganjil=0;
    for(var i in hasil){
      if (hasil[i]%2==0){
        genap++;
      }else if(hasil[i]%2==1){
        ganjil++;
      }
    }
    if(!genap){
      if(!ganjil){
        return "Plat tidak ditemukan";
      }else{
        return "Plat ganjil sebanyak "+ganjil+" dan plat genap tidak ditemukan";
      }
    }else{
      if(!ganjil){
        return"Plat genap sebanyak "+genap+" dan plat ganjil tidak ditemukan";
      }else{
        return"Plat genap sebanyak "+genap+" dan plat ganjil sebanyak "+ganjil;
      }
    }
    // console.log(hasil)
  }else{
    return"Invalid Data";
  }
}
  
  console.log(ganjilGenap('2341;3429;864;1309;1276')) //plat genap sebanyak 2 dan plat ganjil sebanyak 3
  console.log(ganjilGenap('2347;3429;1305')) //plat ganjil sebanyak 3 dan plat genap tidak ditemukan
  console.log(ganjilGenap('864;1308;1276;1432')) //plat genap sebanyak 4 dan plat ganjil tidak ditemukan
  console.log(ganjilGenap('aa')) //plat tidak ditemukan
  console.log(ganjilGenap()) //invalid data
